package controllers;

import play.mvc.Result;
import play.mvc.Results;
import play.routing.Router;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.stream.Collectors;

public class HomeController {

    private final Provider<Router> routerProvider;

    @Inject
    public HomeController(Provider<Router> routerProvider) {
        this.routerProvider = routerProvider;
    }

    public Result index() {
        Router router = routerProvider.get();
        String out = router.documentation().stream()
            .map(doc -> "<a href=\"" + doc.getPathPattern() + "\">" + doc.getPathPattern() + "</a></br>")
            .collect(Collectors.joining("\n"));
        return Results.ok(out).as("text/html");
    }

}
