package utils;

@FunctionalInterface
public interface TailCall<T> {

    TailCall<T> apply();

    default T invoke() {
        TailCall<T> current = this;
        while (!(current instanceof Done)) {
            current = current.apply();
        }
        return ((Done<T>)current).value;
    }

    static <T> TailCall<T> done(T value) {
        return new Done<>(value);
    }

    final class Done<T> implements TailCall<T> {

        private final T value;

        public Done(T value) {
            this.value = value;
        }

        @Override
        public TailCall<T> apply() {
            throw new UnsupportedOperationException();
        }
    }

}
