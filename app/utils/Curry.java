package utils;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Curry {

    public static <A, B, C> CurriedFunction<A, B, C> curry(BiFunction<A, B, C> original) {
        return a -> b -> original.apply(a, b);
    }

    public static <A, B, C, D> CurriedFunction3<A, B, C, D> curry(Function3<A, B, C, D> original) {
        return a -> b -> c -> original.apply(a, b, c);
    }

    @FunctionalInterface
    public interface CurriedFunction<A, B, C> {
        Function<B, C> apply(A a);
    }

    @FunctionalInterface
    public interface Function3<A, B, C, D> {
        D apply(A a, B b, C c);
    }

    @FunctionalInterface
    public interface CurriedFunction3<A, B, C, D> {
        CurriedFunction<B, C, D> apply(A a);
    }

}
