package utils;

import java.util.function.BooleanSupplier;

public class Checks {

    public static <T> void check(boolean condition, String message) {
        if (!condition) {
            throw new IllegalStateException(message);
        }
    }

}
