package models;

import utils.TailCall;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Arrays.copyOfRange;

public abstract class List<T> {

    public static <T> List<T> emptyList() {
        return new Empty<>();
    }

    public static <T> List<T> listOf(T item) {
        return listOf(item, emptyList());
    }

    public static <T> List<T> listOf(T item, List<T> tail) {
        return new NonEmpty<>(item, tail);
    }

    public static <T> List<T> listOf(T... items) {
        switch (items.length) {
            case 0:
                return emptyList();
            case 1:
                return listOf(items[0]);
            default:
                T[] tail = copyOfRange(items, 1, items.length);
                T head = items[0];
                return listOf(head, listOf(tail));
        }

    }

    public final  <R> R fold(R initial, BiFunction<R, T, R> onFold) {
        return foldCall(initial, onFold).invoke();
    }

    protected abstract <R> TailCall<R> foldCall(R initial, BiFunction<R, T, R> onFold);

    public List<T> prepend(T value) {
        return listOf(value, this);
    }

    public List<T> append(T value) {
        return foldRight(listOf(value), List::prepend);
    }

    public abstract List<T> appendAll(List<T> values);

    public <R> List<R> map(Function<T, R> transform) {
        return fold(emptyList(), (list, item) -> list.append(transform.apply(item)));
    }

    public static <T> List<T> flatten(List<List<T>> nestedList) {
        return nestedList.fold(emptyList(), List::appendAll);
    }

    public <R> List<R> flatMap(Function<T, List<R>> transform) {
        return fold(emptyList(), (acc, item) -> acc.appendAll(transform.apply(item)));
    }

    public List<T> filter(Predicate<T> condition) {
        return fold(emptyList(), (acc, next) ->
            condition.test(next) ? acc.append(next) : acc
        );
    }

    public List<T> reverse() {
        return fold(emptyList(), List::prepend);
    }

    public <R> R foldRight(R acc, BiFunction<R, T, R> onFoldRight) {
        return reverse().fold(acc, onFoldRight);
    }

    @Override
    public String toString() {
        return "[" + toStringCall("").invoke() + "]";
    }

    protected abstract TailCall<String> toStringCall(String acc);

    public static final class Empty<T> extends List<T> {

        @Override
        public <R> TailCall<R> foldCall(R initial, BiFunction<R, T, R> onFold) {
            return TailCall.done(initial);
        }

        @Override
        public List<T> appendAll(List<T> values) {
            return values;
        }

        @Override
        protected TailCall<String> toStringCall(String acc) {
            return TailCall.done(acc);
        }
    }

    public static final class NonEmpty<T> extends List<T> {

        private final T head;
        private final List<T> tail;

        public NonEmpty(T head, List<T> tail) {
            this.head = head;
            this.tail = tail;
        }

        @Override
        public <R> TailCall<R> foldCall(R initial, BiFunction<R, T, R> onFold) {
            return () -> tail.foldCall(onFold.apply(initial, head), onFold);
        }

        @Override
        public List<T> appendAll(List<T> values) {
            return foldRight(values, List::prepend); //List.listOf(head, tail.appendAll(values));
        }

        @Override
        protected TailCall<String> toStringCall(String acc) {
            return () -> tail.toStringCall(acc + separatorFor.apply(acc) + head);
        }
    }

    private static final Function<String, String> separatorFor =
        acc -> acc.isEmpty() ? "" : ",";

}
