package models;

public class User {
    private final int id;
    private final String name;
    private final State state;

    public User(int id, String name, State state) {
        this.id = id;
        this.name = name;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public State getState() {
        return state;
    }

    public User asActive() {
        return new User(id, name, State.ACTIVE);
    }

    public User asInactive() {
        return new User(id, name, State.INACTIVE);
    }

    public enum State {
        ACTIVE, INACTIVE;
    }

}
